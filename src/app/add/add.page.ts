import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetController, AlertController, NavController, ToastController } from '@ionic/angular';
import { ContactsService } from '../services/contacts.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  formData: any = {
    name: '',
    wa: '',
    desc: '',
    isFav: false,
  }

  id: any = null

  constructor(
    public cs: ContactsService,
    public navCtrl: NavController,
    private route: ActivatedRoute,
    public alertController: AlertController,
    public toastController: ToastController,
    public actionSheetController: ActionSheetController
  ) {
    this.route.paramMap.subscribe(params => {
      if (params.get("id")) {
        this.id = params.get("id")
        this.formData = this.cs.contacts[this.id]
      }
    })
  }

  ngOnInit() {
  }

  async save() {
    // Deklarasi component alert
    const alert = await this.alertController.create({
      header: 'Simpan Contact',
      message: 'Apakah anda ingin menyimpan contact tersebut?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: async () => {
            // Perintah simpan data contanct
            if (this.id) {
              // fungsi edit
              this.cs.update(this.id, this.formData)
            } else {
              //fungsi add
              this.cs.add(this.formData)
            }

            const toast = await this.toastController.create({
              message: 'Contact berhasil disimpan.',
              duration: 2000,
              mode: 'ios',
              position: 'bottom',
              color: 'dark',
            });
            toast.present();

            this.navCtrl.back()
          }
        }
      ]
    });

    // perintah untuk menampilkan alert
    await alert.present();
  }


  async hub() {
    let contact = this.formData

    let actionSheet = await this.actionSheetController.create({
      header: 'Hubungi Via',
      buttons: [
        {
          text: 'Whatsapp',
          icon: 'logo-whatsapp',
          handler: () => {
            window.open('https://wa.me/' + contact.wa, '_system')
          }
        },
        {
          text: 'Telephone',
          icon: 'call-outline',
          handler: () => {
            window.open('tel://' + contact.wa, '_system')
          }
        },
        {
          text: 'SMS',
          icon: 'mail-outline',
          handler: () => {
            window.open('sms:' + contact.wa, '_system')
          }
        },
        {
          text: 'Batal',
          icon: 'close-outline',
        },
      ]
    })

    await actionSheet.present();
  }

}
