import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { ContactsService } from '../services/contacts.service';

import { Platform } from '@ionic/angular';

import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(
    public router: Router,
    public cs: ContactsService,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    public platform: Platform,
    public statusBar: StatusBar
  ) {
    this.init()
  }

  init() {
    this.platform.ready().then(res => {
      this.statusBar.backgroundColorByHexString('#c2185b')
    })
  }

  add() {
    this.router.navigateByUrl('add')
  }

  edit(id) {
    this.router.navigateByUrl('add/' + id)
  }

  async delete(i) {
    // Deklarasi component alert
    const alert = await this.alertController.create({
      header: 'Hapus Contact',
      message: 'Apakah anda ingin menghapus contact yang terpilih?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            this.cs.delete(i)
          }
        }
      ]
    });

    // perintah untuk menampilkan alert
    await alert.present();
  }

  favorite(i) {
    this.cs.setFavorite(i, true)
  }

  unFavorite(i) {
    this.cs.setFavorite(i, false)
  }

  async hub(i) {
    let contact = this.cs.contacts[i]

    let actionSheet = await this.actionSheetController.create({
      header: 'Hubungi Via',
      buttons: [
        {
          text: 'Whatsapp',
          icon: 'logo-whatsapp',
          handler: () => {
            window.open('https://wa.me/' + contact.wa, '_system')
          }
        },
        {
          text: 'Telephone',
          icon: 'call-outline',
          handler: () => {
            window.open('tel://' + contact.wa, '_system')
          }
        },
        {
          text: 'SMS',
          icon: 'mail-outline',
          handler: () => {
            window.open('sms:' + contact.wa, '_system')
          }
        },
        {
          text: 'Batal',
          icon: 'close-outline',
        },
      ]
    })

    await actionSheet.present();
  }

}
