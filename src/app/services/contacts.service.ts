import { Injectable } from '@angular/core';
import { DbService } from './db.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  contacts: any[] = []

  constructor(
    public db: DbService,
  ) {
    this.contacts = this.db.getData()
  }

  add(contact: any) {
    this.contacts.push(contact)
    this.db.storeData(this.contacts)
  }

  update(index, newContact: any) {
    this.contacts[index] = newContact
    this.db.storeData(this.contacts)
  }

  delete(index) {
    this.contacts.splice(index, 1)
    this.db.storeData(this.contacts)
  }

  setFavorite(index, isFav: boolean) {
    this.contacts[index].isFav = isFav
    this.db.storeData(this.contacts)
  }
}
