import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  constructor() { }

  storeData(contacts: Array<any>) {
    localStorage.setItem('contacts', JSON.stringify(contacts))
  }

  getData() {
    return JSON.parse(localStorage.getItem('contacts'))
  }
}
