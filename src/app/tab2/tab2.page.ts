import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ContactsService } from '../services/contacts.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(
    public router : Router,
    public cs: ContactsService,
  ) {}

  add() {
    this.router.navigateByUrl('add')
  }

  delete(i) {
    this.cs.delete(i)
  }

  unFavorite(i) {
    this.cs.setFavorite(i, false)
  }

}
